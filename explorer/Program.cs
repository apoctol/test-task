﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace explorer
{
    public class Program
    {
        public static string[] GetDisk()
        {
            string[] drivers = Environment.GetLogicalDrives();
            return drivers;

        }
        public static List<string> GetDiskContent(string path)
        {
            string[] cat = Directory.GetDirectories(path);
            string[] files = Directory.GetFiles(path);
            List<string> conc = new List<string>();
            foreach (var item in cat)
            {
                conc.Add(item);
            }
            foreach (var item in files)
            {
                conc.Add(item);
            }
            return conc;
        }
    }
}
